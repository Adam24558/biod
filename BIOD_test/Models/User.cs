﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace BIOD_test.Models
{
    public class User
    {
        public virtual int ID { get; set; }
        public virtual string Login { get; set; }
        public virtual string haslo_1 { get; set; }
        public virtual string haslo_2 { get; set; }
        public virtual string haslo_3 { get; set; }
        public virtual string haslo_4 { get; set; }
        public virtual string haslo_5 { get; set; }
        public virtual string haslo_6 { get; set; }
        public virtual string haslo_7 { get; set; }
        public virtual string haslo_8 { get; set; }
        public virtual string haslo_9 { get; set; }
        public virtual string haslo_10 { get; set; }
        public virtual string haslo_11 { get; set; }
        public virtual string haslo_12 { get; set; }
        public virtual string haslo_13 { get; set; }
        public virtual string haslo_14 { get; set; }
        public virtual string haslo_15 { get; set; }
        public virtual string haslo_16 { get; set; }
        public virtual string haslo_17 { get; set; }
        public virtual string haslo_18 { get; set; }
        public virtual string haslo_19 { get; set; }
        public virtual string haslo_20 { get; set; }

    }
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Id(x => x.ID);
            Map(x => x.Login);
            Map(x => x.haslo_1);
            Map(x => x.haslo_2);
            Map(x => x.haslo_3);
            Map(x => x.haslo_4);
            Map(x => x.haslo_5);
            Map(x => x.haslo_6);
            Map(x => x.haslo_7);
            Map(x => x.haslo_8);
            Map(x => x.haslo_9);
            Map(x => x.haslo_10);
            Map(x => x.haslo_11);
            Map(x => x.haslo_12);
            Map(x => x.haslo_13);
            Map(x => x.haslo_14);
            Map(x => x.haslo_15);
            Map(x => x.haslo_16);
            Map(x => x.haslo_17);
            Map(x => x.haslo_18);
            Map(x => x.haslo_19);
            Map(x => x.haslo_20);  
        }
    }
}