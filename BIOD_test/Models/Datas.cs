﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BIOD_test.Models
{
    public class Datas
    {
        public virtual int ID { get; set; } 
        public virtual User User_id { get; set; }
        public virtual string haslo { get; set; }  

    }
    public class DatasMap : ClassMap<Datas>
    {
        public DatasMap()
        {
            Id(x => x.ID);
            Map(x => x.haslo);
            References<User>(x => x.User_id).Column("User_id").ForeignKey("ID").Fetch.Join();
        }
    }
        }