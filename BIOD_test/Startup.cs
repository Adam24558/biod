﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BIOD_test.Startup))]
namespace BIOD_test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
