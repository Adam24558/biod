﻿using BIOD_test.Models;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BIOD_test.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {

                if (Session["LoginId"] != null && Session["LoginName"] != null)
                {
                    var list = session.Query<Datas>().Where(x => x.User_id.ID == (int)Session["LoginId"] && x.User_id.Login ==(string)Session["LoginName"]).ToList();
                    return View(list);

                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }

                
            }
        }
        public ActionResult Create()
        {
            if (Session["LoginId"] != null)
                {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult Create(Datas data, string id)
        {
            if (Session["LoginId"] != null)
                { 
            using (ISession session = NHibernateHelper.OpenSession())
                {
                    var data1 = new Datas();
                    data1.haslo = data.haslo;
                    data1.User_id = session.Get<User>(Session["LoginId"]);
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Save(data1);
                        transaction.Commit();
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index","Login");
            }
        }
        public ActionResult Delete(int id)
        {
            if (Session["LoginId"] != null)
            {
                using (ISession session = NHibernateHelper.OpenSession())
                {
                    var data1 = session.Get<Datas>(id);
                    return View(data1);
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult Delete(Datas data)
        {
            if (Session["LoginId"] != null)
            {
                using (ISession session = NHibernateHelper.OpenSession())
                {
                    var data1 = session.Get<Datas>(data.ID);
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Delete(data1);
                        transaction.Commit();
                    }
                }

                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }


    }
}