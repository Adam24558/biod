﻿using BIOD_test.Models;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BIOD_test.Controllers
{
    public class LoginController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            Random rnd = new Random();
            var losowanie = 0;
            var licznik = 0;
            var lista = new List<int>();
            var losowanie1 = 3;
            while(losowanie1==3)
            {
                losowanie1= rnd.Next(1, 7);
            }
            lista.Add(losowanie1);
            while (licznik<6)
            {
                losowanie = rnd.Next(1, 21);
                if (losowanie != 3)
                {
                    if (!lista.Contains(losowanie))
                    {
                        lista.Add(losowanie);
                        licznik++;
                    }
                }
            }
           lista.Sort();
            TempData["lista"] = lista;
            ViewBag.losowanie = lista;
            return View();
        }
        //[HttpPost]
        //public ActionResult Index(User user)
        //{

        //    using (ISession session = NHibernateHelper.OpenSession())
        //    {

        //        var user1 = session.Query<User>().Where(p => p.Login.Equals(user.Login) && p.haslo_1.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(user.haslo_1.Trim(), "md5")) && p.Aktywny == true).Fetch(x => x.Rola).FirstOrDefault();

        //    }
        //}
        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            try
            {

                form["login"] = Regex.Replace(form["login"], @"[^a-zA-Z0-9]", "");
                var lista = new List<int>();
                lista.Add(Int32.Parse(form["liczby1"]));
                lista.Add(Int32.Parse(form["liczby2"]));
                lista.Add(Int32.Parse(form["liczby3"]));
                lista.Add(Int32.Parse(form["liczby4"]));
                lista.Add(Int32.Parse(form["liczby5"]));
                lista.Add(Int32.Parse(form["liczby6"]));
                lista.Add(Int32.Parse(form["liczby7"]));

                var login = form["Login"];
                var licznik = 0;
                using (ISession session = NHibernateHelper.OpenSession())
                {
                    foreach (var item in lista)
                    {
                        if (item == 1)
                        {
                            if (form["haslo_1"] == "")
                            {
                                form["haslo_1"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_1.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_1"].Trim(), "md5"))).FirstOrDefault();
                           if(uzytkownik!=null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 2)
                        {
                            if (form["haslo_2"] == "")
                            {
                                form["haslo_2"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_2.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_2"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 3)
                        {
                            if (form["haslo_3"] == "")
                            {
                                form["haslo_3"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_3.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_3"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 4)
                        {
                            if (form["haslo_4"] == "")
                            {
                                form["haslo_4"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_4.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_4"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 5)
                        {
                            if (form["haslo_5"] == "")
                            {
                                form["haslo_5"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_5.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_5"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 6)
                        {
                            if (form["haslo_6"] == "")
                            {
                                form["haslo_6"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_6.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_6"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 7)
                        {
                            if (form["haslo_7"] == "")
                            {
                                form["haslo_7"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_7.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_7"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 8)
                        {
                            if (form["haslo_8"] == "")
                            {
                                form["haslo_8"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_8.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_8"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 9)
                        {

                            if (form["haslo_9"] == "")
                            {
                                form["haslo_9"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_9.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_9"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 10)
                        {

                            if (form["haslo_10"] == "")
                            {
                                form["haslo_10"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_10.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_10"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 11)
                        {
                            if (form["haslo_11"] == "")
                            {
                                form["haslo_11"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_11.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_11"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 12)
                        {
                            if (form["haslo_12"] == "")
                            {
                                form["haslo_12"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_12.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_12"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 13)
                        {
                            if (form["haslo_13"] == "")
                            {
                                form["haslo_13"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_13.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_13"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 14)
                        {
                            if (form["haslo_14"] == "")
                            {
                                form["haslo_14"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_14.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_14"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 15)
                        {
                            if (form["haslo_15"] == "")
                            {
                                form["haslo_15"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_15.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_15"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 16)
                        {
                            if (form["haslo_16"] == "")
                            {
                                form["haslo_16"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_16.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_16"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 17)
                        {
                            if (form["haslo_17"] == "")
                            {
                                form["haslo_17"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_17.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_17"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 18)
                        {
                            if (form["haslo_18"] == "")
                            {
                                form["haslo_18"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_18.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_18"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 19)
                        {
                            if (form["haslo_19"] == "")
                            {
                                form["haslo_19"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_19.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_19"].Trim(), "md5"))).FirstOrDefault();
                            if (uzytkownik != null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                        if (item == 20)
                        {
                            if (form["haslo_20"] == "")
                            {
                                form["haslo_20"] = " ";
                            }
                            var uzytkownik = session.Query<User>().Where(x => x.Login == login && x.haslo_20.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(form["haslo_20"].Trim(), "md5"))).FirstOrDefault();
                             if(uzytkownik!=null)
                            {
                                licznik++;
                            }
                            continue;
                        }
                    }
                    if (licznik == 7)
                    {
                        var uzytkownik = session.Query<User>().Where(x => x.Login == login).FirstOrDefault();
                        Session["LoginId"] = uzytkownik.ID;
                        Session["LoginName"] = uzytkownik.Login;
                        Session["SesjaId"] = HttpContext.Session.SessionID;
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }

                }
            }
            catch
            {
                return RedirectToAction("Index", "Login");
            }


           
        }
        public ActionResult Registery()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registery(FormCollection form)
        {
            var capcha = form["g-recaptcha-response"];
            if (capcha == "")
            {
                TempData["recaptcha"] = "Proszę zweryfikować czy nie jesteś botem";
               
            }
            else
            { 
                form["login"] = Regex.Replace(form["login"], @"[^a-zA-Z0-9\-@_]", "");
                form["haslo"] = Regex.Replace(form["haslo"], @"[^a-zA-Z0-9\-@_]", "");
                using (ISession session = NHibernateHelper.OpenSession())
                {
                    var login = session.Query<User>().Where(x => x.Login == form["login"]).FirstOrDefault();
                    if (form["haslo"].Length < 9)
                    {
                        TempData["Haslo"] = "Haslo musi posiadać więcej niż 8 znaków oraz nie może zawierać zanków specjalnych tj.(%./@ itp.)";

                    }
                   
                    if (login != null)
                    {
                        TempData["Login"] = "Ten login jest już zajęty";
                    }

                    if (TempData["Haslo"] != null || TempData["Login"] != null)
                    {
                        return RedirectToAction("Registery");
                    }



                    var user = new User();
                        user.Login = form["login"];
                        char[] tab = new char[20];
                        var licznik = 0;
                        var haslo = form["haslo"];
                        for (var i = 0; i < haslo.Length; i++)
                        {
                            licznik++;
                            tab[i] = haslo[i];
                        }

                        if (licznik < 20)
                        {
                            for (var i = licznik; i < 20; i++)
                            {
                                tab[i] = ' ';
                            }
                        }
                        user.haslo_1 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[0].ToString().Trim(), "md5");
                        user.haslo_2 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[1].ToString().Trim(), "md5");
                        user.haslo_3 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[2].ToString().Trim(), "md5");
                        user.haslo_4 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[3].ToString().Trim(), "md5");
                        user.haslo_5 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[4].ToString().Trim(), "md5");
                        user.haslo_6 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[5].ToString().Trim(), "md5");
                        user.haslo_7 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[6].ToString().Trim(), "md5");
                        user.haslo_8 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[7].ToString().Trim(), "md5");
                        user.haslo_9 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[8].ToString().Trim(), "md5");
                        user.haslo_10 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[9].ToString().Trim(), "md5");
                        user.haslo_11 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[10].ToString().Trim(), "md5");
                        user.haslo_12 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[11].ToString().Trim(), "md5");
                        user.haslo_13 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[12].ToString().Trim(), "md5");
                        user.haslo_14 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[13].ToString().Trim(), "md5");
                        user.haslo_15 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[14].ToString().Trim(), "md5");
                        user.haslo_16 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[15].ToString().Trim(), "md5");
                        user.haslo_17 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[16].ToString().Trim(), "md5");
                        user.haslo_18 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[17].ToString().Trim(), "md5");
                        user.haslo_19 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[18].ToString().Trim(), "md5");
                        user.haslo_20 = FormsAuthentication.HashPasswordForStoringInConfigFile(tab[19].ToString().Trim(), "md5");
                        using (ITransaction transaction = session.BeginTransaction())
                        {
                            session.Save(user);
                            transaction.Commit();
                        }

                    var uzytkownik = session.Query<User>().Where(x => x.Login == form["login"]).FirstOrDefault();
                    Session["LoginId"] = uzytkownik.ID;
                    Session["LoginName"] = uzytkownik.Login;
                    Session["SesjaId"] = HttpContext.Session.SessionID;
                    return RedirectToAction("Index", "Home");

                }




                
          
                
               
               
            }


            return RedirectToAction("Index");


        }
        public ActionResult Logout()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {

                Session["LoginId"] = null;
                Session["LoginName"] = null;
                Session["SesjaId"] = null;
               




            }
            return RedirectToAction("Index", "Login");
        }
    }
}